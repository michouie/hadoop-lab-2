package hadoop;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MapReduce {

    /**
     * CountGenderProportionMapper
     * Mapper to count the proportion of gender (male only, female only or gender-neutral)
     * For a given line, this function returns several sets of (type of gender, 1) or (type of gender, 0)
     * it returns a value 1 if the key is true for the first name type,
     * it returns a value 0 if the key is false for the first name gender  
     * @param key Object : key given to each line
     * @param value Text : line that will be mapped
     * @return word Text : a type of gender 
     * @return one or zero IntWritable : the value 1 or 0
     */
    public static class CountGenderProportionMapper
            extends Mapper<Object, Text, Text, IntWritable>{

        // key : gender 
        private Text gender = new Text();
        // value : one if the first name of the line matchs the gender  
        private final static IntWritable one = new IntWritable(1);
        // value : zero if the first name of the line does not match the gender  
        private final static IntWritable zero = new IntWritable(0);
        
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            String[] col = value.toString().split(";"); //we split the line with the delimiter ";"
            String[] genders = col[1].toString().split(","); // We split the genders with the delimiter "," to get all the genders for a first name
            if (genders.length>1){  //if the name has more than 1 gender
                gender.set("gender-neutral name");
                context.write(gender, one); // it is a neutral name (male or female)
                gender.set("only female name");
                context.write(gender, zero); // it is not only a female name, but also male
                gender.set("only male name");
                context.write(gender, zero); // it is not only a male name, but also female
            } else if (genders[0].equals("m")){
                gender.set("gender-neutral name");
                context.write(gender, zero); // it is not a neutral name
                gender.set("only female name");
                context.write(gender, zero); // it is not a female name
                gender.set("only male name");
                context.write(gender, one); // it is a male name
            } else if (genders[0].equals("f")){
                gender.set("gender-neutral name");
                context.write(gender, zero); // it is a not a neutral name
                gender.set("only female name");
                context.write(gender, one); // it is a female name
                gender.set("only male name");
                context.write(gender, zero); // it is not a male name
            }
        }
    }

    /**
     * CountGenderProportionReducer
     * Reducer to count the proportion of a gender 
     * For a gender, it returns a proportion (type of gender, proportion)
     * @param  key Text : a gender 
     * @param  values array of IntWritable : array of the value 1 and 0  
     * @return key Text : a gender 
     * @return result DoubleWritable : proportion of the gender 
     */
    public static class CountGenderProportionReducer
            extends Reducer<Text,IntWritable,Text,DoubleWritable> {
        
        // value : proportion of a gender
        private DoubleWritable result = new DoubleWritable();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            double sum = 0.0;
            double total = 0.0;
            for (IntWritable val : values) {
                sum += val.get(); // the value can be increase by 1 or by 0
                total +=1; // count the total number of name to compute the proportion
            }
            result.set(sum*100/total); // set the result to the proportion of gender
            context.write(key, result); // the set (type of gender, proportion)
        }
    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "count the proportion of a gender");
        job.setJarByClass(MapReduce.class); // define which class will be used for the jar
        job.setMapperClass(CountGenderProportionMapper.class); // specify the Mapper to use
        job.setMapOutputKeyClass(Text.class); // specify the class of the output key for the mapper
        job.setMapOutputValueClass(IntWritable.class); // specify the class of the output value for the mapper
        job.setReducerClass(CountGenderProportionReducer.class); // specify the Reducer to use
        job.setOutputKeyClass(Text.class); // specify the class of the output key (for the reducer only here)
        job.setOutputValueClass(DoubleWritable.class); // specify the class of the output value (for the reducer only here)
        FileInputFormat.addInputPath(job, new Path(args[0])); //specify the input 
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // specify the output
        System.exit(job.waitForCompletion(true) ? 0 : 1); // depends on the output true or false, the exit code will be set to 0 or 1
    }
}
