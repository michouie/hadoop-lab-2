## Lab 2 HADOOP Map/Reduce ##

### Usage ###

Use one of the jar provided one this repo or on the cluster to launch the algorithms.
The following command needs to be used:

```bash
yarn jar [PATH_TO_JAR] hadoop.MapReduce /res/prenoms.csv [OUTPUT]
```

The output result will be in a file called part-r-00000 in the [OUTPUT] folder.


### Author ###

Michelle Deng
