package hadoop;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MapReduce {

    /**
     * CountNbNameByNbOriginMapper
     * Mapper to count the number of first name by number of origins 
     * for a given line, this function returns the number of origin for the name (number of origins, 1)
     * @param key Object : key given to each line
     * @param value Text : line that will be mapped 
     * @return nbOrigins IntWritable : number of origins in a line (for a name) 
     * @return one IntWritable : the value 1 
     */
    public static class CountNbNameByNbOriginMapper
            extends Mapper<Object, Text, IntWritable, IntWritable>{

        // key : number of origins that will be return by the mapper
        private IntWritable nbOrigins = new IntWritable();
        // value : one that will be return by the mapper
        private final static IntWritable one = new IntWritable(1);
        
     
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            String[] col = value.toString().split(";"); //we split the line with the delimiter ";"
            col[2]  = col[2].replaceAll(", ", ",");  // we replace the pattern ", " to ","
            String[] origins = col[2].toString().split(","); // We split the origins with the delimiter "," to get each origin in a line
            if (!origins[0].isEmpty() && !origins[0].equals("?")){
                nbOrigins.set(origins.length); // the "nbOrigins" is set to the length of the array of origins 
                context.write(nbOrigins, one); // the set (number of origins, 1) is sent to the context Hadoop
            }
        }
    }

    /**
     * CountNbNameByNbOriginReducer
     * Reducer to count the number of first name by number of origins
     * For a number of origins, this function returns a set of (number of origins, number of first name)
     * @param  key IntWritable : number of origins 
     * @param  values array of IntWritable : array of the value 1 
     * @return key Text : the numbers of origin 
     * @return result Text : the numbers of first names given by the sum of 1 in the array of IntWritable given in param 
     */
    public static class CountNbNameByNbOriginReducer
            extends Reducer<IntWritable,IntWritable,Text,Text> {
      
        // key : number of origins that will be return
        private Text resultKey = new Text();
        //value : number of first names that will be return 
        private Text resultValue = new Text();

        public void reduce(IntWritable key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) { // sum up the number of the value 1 in the array of IntWritable
                sum += val.get();
            }
            resultKey.set(key + " origin(s) "); // set the result key with the pattern "X origin(s)"
            resultValue.set(sum + " first name(s)"); // set the result value with the pattern "Y first name(s)"
            context.write(resultKey, resultValue); // the set (X origin(s), Y first name(s)) is sent to the context hadoop
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "count number of first name by number of origin");
        job.setJarByClass(MapReduce.class); // define which class will be used for the jar
        job.setMapperClass(CountNbNameByNbOriginMapper.class); // specify the Mapper to use
        job.setMapOutputKeyClass(IntWritable.class); // specify the class of the output key for the mapper only 
        job.setMapOutputValueClass(IntWritable.class); // specify the class of the output value for the mapper only 
        job.setReducerClass(CountNbNameByNbOriginReducer.class); // specify the Reducer to use
        job.setOutputKeyClass(Text.class); // specify the class of the output key (for the reducer only here)
        job.setOutputValueClass(Text.class); // specify the class of the output value (for the reducer only here)
        FileInputFormat.addInputPath(job, new Path(args[0])); //specify the input
        FileOutputFormat.setOutputPath(job, new Path(args[1])); //specify the output
        System.exit(job.waitForCompletion(true) ? 0 : 1); // depends on the output true or false, the exit code will be set to 0 or 1
    }
}
