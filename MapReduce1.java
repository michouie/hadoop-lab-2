package hadoop;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MapReduce {

    /**
     * CountByOriginMapper 
     * Mapper to count the number of first name by origin 
     * for a given line, this function returns one or several sets of (origin,1)
     * @param key Object : key given to each line
     * @param value Text : line that will be mapped
     * @return word Text : an origin 
     * @return  one IntWritable : the value 1 
     */
    public static class CountByOriginMapper
            extends Mapper<Object, Text, Text, IntWritable>{  

        // key : origin that will be return by the mapper 
        private Text word = new Text();
        // value : one that will be return by the mapper 
        private final static IntWritable one = new IntWritable(1);
        
        
        public void map(Object key, Text value, Context context 
        ) throws IOException, InterruptedException {
            String[] col = value.toString().split(";"); //we split the line with the delimiter ";"
            col[2]  = col[2].replaceAll(", ", ",");  // we replace the pattern ", " to ","
            String[] origins = col[2].toString().split(","); // We split the origins with the delimiter "," to get each origin in a line
            for (String anOrigin : origins) { // for each origin (in the line)
                if (!anOrigin.isEmpty() && !anOrigin.equals("?")) { // In the case it is not an empty string or the string is not equal to "?"
                    word.set(anOrigin); // The "word" object is set to an origin
                    context.write(word, one); // the set (origin, 1) is sent to the context Hadoop 
                }
            }
        }
    }

    /**
     * CountByOriginReducer
     * Reducer to count the number of first name for an origin 
     * For an origin, this function returns a set of (origin, number of first name)
     * @param  key Text : an origin 
     * @param  values array of IntWritable : array of the value 1 
     * @return key Text : an origin (the same than the key param)
     * @return result IntWritable : sum of the value 1 in the array of IntWritable given in param 
     */
    public static class CountByOriginReducer
            extends Reducer<Text,IntWritable,Text,IntWritable> {

        // value : number of first name for an origin        
        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) { // loop in the array to sum up all its values 
                sum += val.get();
            }
            result.set(sum); // set the "result" to the number of first name 
            context.write(key, result); // the set (origin, number of first name) is sent to the context hadoop
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration(); 
        Job job = Job.getInstance(conf, "count first name by origin"); 
        job.setJarByClass(MapReduce.class); // define which class will be used for the jar
        job.setMapperClass(CountByOriginMapper.class); // specify the Mapper to use
        job.setReducerClass(CountByOriginReducer.class); // specify the Reducer to use
        job.setOutputKeyClass(Text.class); // specify the class of the output key for both the mapper and the reducer 
        job.setOutputValueClass(IntWritable.class); // specify the class of the output value for both the mapper and the reducer 
        FileInputFormat.addInputPath(job, new Path(args[0])); //specify the input 
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // specify the output
        System.exit(job.waitForCompletion(true) ? 0 : 1); // depends on the output true or false, the exit code will be set to 0 or 1
    }
}